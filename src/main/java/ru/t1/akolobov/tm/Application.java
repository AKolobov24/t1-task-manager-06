package ru.t1.akolobov.tm;

import java.util.Scanner;

import static ru.t1.akolobov.tm.constant.ArgumentConst.*;
import static ru.t1.akolobov.tm.constant.TerminalConst.*;

public class Application {

    private static boolean isCommandMode = false;

    public static void main(String[] args) {
        processArguments(args);
        isCommandMode = true;
        displayWelcome();
        porcessCommands();
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        final String arg = args[0];
        processArgument(arg);
        exit();
    }

    private static void porcessCommands() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!CMD_EXIT.equals(command)) {
            System.out.println("ENTER COMMAND:");
            command = scanner.nextLine();
            processCommand(command);
            System.out.println();
        }
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) {
            displayError();
            return;
        }
        switch (command) {
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_EXIT:
                exit();
                break;
            default:
                displayError();
        }
    }

    private static void processArgument(final String arg) {
        switch (arg) {
            case ARG_VERSION:
                displayVersion();
                break;
            case ARG_ABOUT:
                displayAbout();
                break;
            case ARG_HELP:
                displayHelp();
                break;
            default:
                displayError();
        }
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Display application version. \n", CMD_VERSION, ARG_VERSION);
        System.out.printf("%s, %s - Display developer info. \n", CMD_ABOUT, ARG_ABOUT);
        System.out.printf("%s, %s - Display list of terminal commands.\n", CMD_HELP, ARG_HELP);
        System.out.printf("%s - Exit application.\n", CMD_EXIT);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.printf("Developer: %s \n", "Alexander Kolobov");
        System.out.printf("e-mail: %s\n", "akolobov@t1-consulting.ru");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayError() {
        System.err.printf("Error! This %s is not supported. \nUse '%s' to display available arguments.\n",
                isCommandMode ? "command" : "argument",
                isCommandMode ? CMD_HELP : ARG_HELP);
    }

}
